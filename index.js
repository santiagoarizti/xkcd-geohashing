#!/usr/bin/env node

//var app = require('./dist/bundle.js');

const express = require('express');
const app = express();
const path = require('path');

app.get('/hello', (req, res) => res.send('Hello World!'));
app.get('/', (req, res) => res.sendFile(path.resolve(__dirname, 'index.html')));
app.get('/dist/:file', (req, res) => res.sendFile(path.resolve(__dirname, 'dist/' + req.params.file)));

app.listen(8080, () => console.log('geohashing started on 8080'));



