import djia from 'djia';
import moment from 'moment';
import Q from 'q';
import md5 from 'md5';

class gdl {
    init() {
        this.msgElement = document.getElementById("msg");
        this.shortestElement = document.getElementById("shortest");
        document.getElementById("do").addEventListener("click", () => this.do());

        this.gdlLocation = {
            lat: 20,
            lon: -103
        };

        this.messFrias = {
            lat: 20.6823321,
            lon: -103.3599151
        }

        this.shortest = {
            lat: 100,
            lon: -100,
            distance: 100,
            date: "00"    
        };
        this.shortestElement.innerText = `Shortest: ${this.shortest.distance}`;
    }

    do() {

        for (let i = 0; i < 365; i++) {
            let dat = moment().subtract(i, 'days').format("YYYY-MM-DD");

            Q.nfcall(djia, {date: dat, cache: true})
                .then(djval => {
                    
                    const hashParts = this.calcHash(dat, djval);
                    //this.msg(`Part1: ${hashParts[0]}, Part2: ${hashParts[1]}`);
                    const hashDecParts = hashParts.map(this.parseFloat)
                    //this.msg(`Part1: ${hashDecParts[0]}, Part2: ${hashDecParts[1]}`);

                    let meetLoc = {
                        lat: "" + this.gdlLocation.lat + ("." + hashDecParts[0]).substr(0, 7),
                        lon: "" + this.gdlLocation.lon + ("." + hashDecParts[1]).substr(0, 7)
                    };

                    //this.msg(JSON.stringify(meetLoc));

                    this.msg(this.getHref(meetLoc));

                    const distance = this.calcDistance(meetLoc);
                    this.msg("Distance: " + distance);

                    this.verifyShortest(meetLoc, distance, dat);
                })
                .catch(err => console.error(err));

        }

    }

    getHref(loc) {
        const href = `https://www.google.com/maps/@${loc.lat},${loc.lon},12.77z`;
        return `<a href="${href}">${href}</a>`;
    }

    verifyShortest(loc, distance, dat) {
        if (this.shortest.distance > distance) {
            this.shortest.distance = distance;
            this.shortest.lat = loc.lat * 1;
            this.shortest.lon = loc.lon * 1;
            this.shortest.dat = dat;
            this.shortestElement.innerText = `Shortest: ${distance} ${this.getHref(loc)} date: ${dat}`;
        }
    }

    calcDistance(meetLoc) {
        const y2 = this.messFrias.lat;
        const x2 = this.messFrias.lon;
        const y1 = meetLoc.lat * 1;
        const x1 = meetLoc.lon * 1;

        const dist = Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));

        return dist;
    }

    calcHash(dat, djval) {

        const val = md5(dat + "-" + djval);

        return [
            val.substr(0, 16),
            val.substr(16, 16)
        ];
    }

    msg(message) {
        this.msgElement.innerHTML += message + "\n"; 
    }

    parseFloat(str) {
        const radix = 16;
        var parts = str.split(".");
        if ( parts.length > 1 ) {
            return parseInt(parts[0], radix) + parseInt(parts[1], radix) / Math.pow(radix, parts[1].length);
        }
        return parseInt(parts[0], radix);
    }
}

export default gdl;
