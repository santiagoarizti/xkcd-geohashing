Results
=======

Here are the results for the Mess Frias bulding:


<pre>
Shortest: 0.023736974137832878 <a href="https://www.google.com/maps/@20.673399,-103.381907,12.77z">https://www.google.com/maps/@20.673399,-103.381907,12.77z</a> date: 2018-01-22
https://www.google.com/maps/@20.190389,-103.143448,12.77z
Distance: 0.5374625745296274
https://www.google.com/maps/@20.589681,-103.127849,12.77z
Distance: 0.2498777723216272
https://www.google.com/maps/@20.640857,-103.267994,12.77z
Distance: 0.10084479433872
https://www.google.com/maps/@20.594417,-103.122997,12.77z
Distance: 0.252703879898229
https://www.google.com/maps/@20.147814,-103.110467,12.77z
Distance: 0.5898592661145684
https://www.google.com/maps/@20.207053,-103.139033,12.77z
Distance: 0.5240983924772336
https://www.google.com/maps/@20.142992,-103.114597,12.77z
Distance: 0.5925105177594878
https://www.google.com/maps/@20.450804,-103.827249,12.77z
Distance: 0.5215421700867717
https://www.google.com/maps/@20.127838,-103.168122,12.77z
Distance: 0.5867267678761714
https://www.google.com/maps/@20.106882,-103.105013,12.77z
Distance: 0.6293789781796172
https://www.google.com/maps/@20.887446,-103.714633,12.77z
Distance: 0.4097517547901757
https://www.google.com/maps/@20.197264,-103.955864,12.77z
Distance: 0.7684049408019393
https://www.google.com/maps/@20.459670,-103.655204,12.77z
Distance: 0.36982961649876284
https://www.google.com/maps/@20.937717,-103.169487,12.77z
Distance: 0.3185660189311099
https://www.google.com/maps/@20.638760,-103.134371,12.77z
Distance: 0.2297143202832964
https://www.google.com/maps/@20.384920,-103.124599,12.77z
Distance: 0.3792461260785868
https://www.google.com/maps/@20.103959,-103.441437,12.77z
Distance: 0.5840901154644034
https://www.google.com/maps/@20.623827,-103.464094,12.77z
Distance: 0.1194825925866267
https://www.google.com/maps/@20.159873,-103.872965,12.77z
Distance: 0.7322456630549733
https://www.google.com/maps/@20.149448,-103.666418,12.77z
Distance: 0.6147434357040499
https://www.google.com/maps/@20.956639,-103.752731,12.77z
Distance: 0.4791123111342697
https://www.google.com/maps/@20.866194,-103.178781,12.77z
Distance: 0.25809835422648075
https://www.google.com/maps/@20.430366,-103.114133,12.77z
Distance: 0.35198828990411046
https://www.google.com/maps/@20.179109,-103.183961,12.77z
Distance: 0.5330978650120624
https://www.google.com/maps/@20.122173,-103.407328,12.77z
Distance: 0.5621620766284586
https://www.google.com/maps/@20.255799,-103.521911,12.77z
Distance: 0.4562599664801009
https://www.google.com/maps/@20.144148,-103.137831,12.77z
Distance: 0.5822056964386503
https://www.google.com/maps/@20.863596,-103.111329,12.77z
Distance: 0.307655083748699
https://www.google.com/maps/@20.131701,-103.928521,12.77z
Distance: 0.7915221271714583
https://www.google.com/maps/@20.938482,-103.112930,12.77z
Distance: 0.3558291878022585
https://www.google.com/maps/@20.123689,-103.841721,12.77z
Distance: 0.7377120294887652
https://www.google.com/maps/@20.725645,-103.135999,12.77z
Distance: 0.2280667164353855
https://www.google.com/maps/@20.114498,-103.151222,12.77z
Distance: 0.6049697307389973
https://www.google.com/maps/@20.156835,-103.669392,12.77z
Distance: 0.6098550268236077
https://www.google.com/maps/@20.852328,-103.650190,12.77z
Distance: 0.33638983870922734
https://www.google.com/maps/@20.977541,-103.135518,12.77z
Distance: 0.37081309729783657
https://www.google.com/maps/@20.166063,-103.518016,12.77z
Distance: 0.5399348832920701
https://www.google.com/maps/@20.154799,-103.900457,12.77z
Distance: 0.7552990912553967
https://www.google.com/maps/@20.372727,-103.159737,12.77z
Distance: 0.36868223399780803
https://www.google.com/maps/@20.744701,-103.255373,12.77z
Distance: 0.12173302904149562
https://www.google.com/maps/@20.453411,-103.971564,12.77z
Distance: 0.6530844102383914
https://www.google.com/maps/@20.129861,-103.100274,12.77z
Distance: 0.6104406745494765
https://www.google.com/maps/@20.168582,-103.285919,12.77z
Distance: 0.5190516236996254
https://www.google.com/maps/@20.174917,-103.117169,12.77z
Distance: 0.5624906690543541
https://www.google.com/maps/@20.678242,-103.169244,12.77z
Distance: 0.19071496347485614
https://www.google.com/maps/@20.893503,-103.246052,12.77z
Distance: 0.23991238931830322
https://www.google.com/maps/@20.485490,-103.747609,12.77z
Distance: 0.4348026821785052
https://www.google.com/maps/@20.695433,-103.129305,12.77z
Distance: 0.23098192960233205
https://www.google.com/maps/@20.175925,-103.162168,12.77z
Distance: 0.5436470054077562
https://www.google.com/maps/@20.276325,-103.122867,12.77z
Distance: 0.47014207104237965
https://www.google.com/maps/@20.118665,-103.169232,12.77z
Distance: 0.5950467580350475
https://www.google.com/maps/@20.120065,-103.907618,12.77z
Distance: 0.7849348752672571
https://www.google.com/maps/@20.881786,-103.428255,12.77z
Distance: 0.2108369041634325
https://www.google.com/maps/@20.333682,-103.666150,12.77z
Distance: 0.46404386237512557
https://www.google.com/maps/@20.659993,-103.908543,12.77z
Distance: 0.5490825147891887
https://www.google.com/maps/@20.114817,-103.159452,12.77z
Distance: 0.6018794257902648
https://www.google.com/maps/@20.139612,-103.428889,12.77z
Distance: 0.547085464827225
https://www.google.com/maps/@20.453306,-103.612764,12.77z
Distance: 0.3411532217529555
https://www.google.com/maps/@20.138853,-103.855623,12.77z
Distance: 0.7355921793080853
https://www.google.com/maps/@20.354944,-103.600735,12.77z
Distance: 0.40641997029873267
https://www.google.com/maps/@20.144961,-103.714952,12.77z
Distance: 0.6440643597318694
https://www.google.com/maps/@20.110718,-103.309635,12.77z
Distance: 0.5738211984362561
https://www.google.com/maps/@20.180796,-103.447507,12.77z
Distance: 0.5091274894845301
https://www.google.com/maps/@20.111655,-103.107041,12.77z
Distance: 0.6241936101204666
https://www.google.com/maps/@20.519475,-103.104419,12.77z
Distance: 0.30298629034268376
https://www.google.com/maps/@20.148634,-103.528785,12.77z
Distance: 0.559777370987449
https://www.google.com/maps/@20.100870,-103.863170,12.77z
Distance: 0.7690017347902547
https://www.google.com/maps/@20.806930,-103.907587,12.77z
Distance: 0.561666401642499
https://www.google.com/maps/@20.742953,-103.120368,12.77z
Distance: 0.24709857675676317
https://www.google.com/maps/@20.657519,-103.103254,12.77z
Distance: 0.2578577324510821
https://www.google.com/maps/@20.167609,-103.107971,12.77z
Distance: 0.5730756487571388
https://www.google.com/maps/@20.673399,-103.381907,12.77z
Distance: 0.023736974137832878
https://www.google.com/maps/@20.935506,-103.119102,12.77z
Distance: 0.34941089389545166
https://www.google.com/maps/@20.912711,-103.153988,12.77z
Distance: 0.3089990421985469
https://www.google.com/maps/@20.173080,-103.107479,12.77z
Distance: 0.5683851563311798
https://www.google.com/maps/@20.125531,-103.163466,12.77z
Distance: 0.5904402711977051
https://www.google.com/maps/@20.145004,-103.166090,12.77z
Distance: 0.5712176961891317
https://www.google.com/maps/@20.821667,-103.787795,12.77z
Distance: 0.44999491461796215
https://www.google.com/maps/@20.147980,-103.129427,12.77z
Distance: 0.581942377745438
https://www.google.com/maps/@20.705954,-103.784114,12.77z
Distance: 0.424856094367053
https://www.google.com/maps/@20.636910,-103.655842,12.77z
Distance: 0.2993925471885148
https://www.google.com/maps/@20.113883,-103.451695,12.77z
Distance: 0.5758106714318688
https://www.google.com/maps/@20.750106,-103.177240,12.77z
Distance: 0.19484222766438197
https://www.google.com/maps/@20.767671,-103.506390,12.77z
Distance: 0.1695217513572236
https://www.google.com/maps/@20.117258,-103.177396,12.77z
Distance: 0.5938198046172071
https://www.google.com/maps/@20.164009,-103.145812,12.77z
Distance: 0.5608020804376666
https://www.google.com/maps/@20.922233,-103.106946,12.77z
Distance: 0.3486342028195455
https://www.google.com/maps/@20.596493,-103.109236,12.77z
Distance: 0.26496860618877066
https://www.google.com/maps/@20.154946,-103.165704,12.77z
Distance: 0.5620089410644792
https://www.google.com/maps/@20.105244,-103.176448,12.77z
Distance: 0.6055500408257122
https://www.google.com/maps/@20.777864,-103.117118,12.77z
Distance: 0.2609152653372649
https://www.google.com/maps/@20.177548,-103.130388,12.77z
Distance: 0.5545175175296247
https://www.google.com/maps/@20.407683,-103.157417,12.77z
Distance: 0.3412295541632064
https://www.google.com/maps/@20.238565,-103.132977,12.77z
Distance: 0.4984276680462453
https://www.google.com/maps/@20.158807,-103.100303,12.77z
Distance: 0.584360310764189
https://www.google.com/maps/@20.151576,-103.254045,12.77z
Distance: 0.5412120802062897
https://www.google.com/maps/@20.415918,-103.813123,12.77z
Distance: 0.5257127288750273
https://www.google.com/maps/@20.282913,-103.173885,12.77z
Distance: 0.4406164040872947
https://www.google.com/maps/@20.973550,-103.149249,12.77z
Distance: 0.3594274210040444
https://www.google.com/maps/@20.102014,-103.116550,12.77z
Distance: 0.6292818677553129
https://www.google.com/maps/@20.707564,-103.418898,12.77z
Distance: 0.06415318596936953
https://www.google.com/maps/@20.116654,-103.565237,12.77z
Distance: 0.6017879987497424
https://www.google.com/maps/@20.152824,-103.762833,12.77z
Distance: 0.6653733253640588
https://www.google.com/maps/@20.118956,-103.487847,12.77z
Distance: 0.5777189637607727
https://www.google.com/maps/@20.154905,-103.179954,12.77z
Distance: 0.5572838983207936
https://www.google.com/maps/@20.115951,-103.398677,12.77z
Distance: 0.5677059408961843
https://www.google.com/maps/@20.222759,-103.116762,12.77z
Distance: 0.5199335190995291
https://www.google.com/maps/@20.154679,-103.152190,12.77z
Distance: 0.5670692295563342
https://www.google.com/maps/@20.173412,-103.769108,12.77z
Distance: 0.6530225858225942
https://www.google.com/maps/@20.180702,-103.291683,12.77z
Distance: 0.5062493226626761
https://www.google.com/maps/@20.640466,-103.740583,12.77z
Distance: 0.38296321026911256
https://www.google.com/maps/@20.944479,-103.413515,12.77z
Distance: 0.2675704513948085
https://www.google.com/maps/@20.829382,-103.937099,12.77z
Distance: 0.5956214632711194
https://www.google.com/maps/@20.352201,-103.564724,12.77z
Distance: 0.3885012595943812
https://www.google.com/maps/@20.115343,-103.483088,12.77z
Distance: 0.5802139284895019
https://www.google.com/maps/@20.106894,-103.523520,12.77z
Distance: 0.5982437381499472
https://www.google.com/maps/@20.158695,-103.179422,12.77z
Distance: 0.5538714396356031
https://www.google.com/maps/@20.109351,-103.858620,12.77z
Distance: 0.7596143220353515
https://www.google.com/maps/@20.231988,-103.176276,12.77z
Distance: 0.4863467152696896
https://www.google.com/maps/@20.803353,-103.400410,12.77z
Distance: 0.12761620258736828
https://www.google.com/maps/@20.170668,-103.117123,12.77z
Distance: 0.5663463207713239
https://www.google.com/maps/@20.152709,-103.252531,12.77z
Distance: 0.5403998269674188
https://www.google.com/maps/@20.986153,-103.355719,12.77z
Distance: 0.30384987499095834
https://www.google.com/maps/@20.657494,-103.112016,12.77z
Distance: 0.24914031185743302
https://www.google.com/maps/@20.828100,-103.125906,12.77z
Distance: 0.2756964627143721
https://www.google.com/maps/@20.910039,-103.163129,12.77z
Distance: 0.3009571422326099
https://www.google.com/maps/@20.674948,-103.807537,12.77z
Distance: 0.4476828009790205
https://www.google.com/maps/@20.104424,-103.513319,12.77z
Distance: 0.5979218415318329
https://www.google.com/maps/@20.167686,-103.175120,12.77z
Distance: 0.5468179196306717
https://www.google.com/maps/@20.113453,-103.681549,12.77z
Distance: 0.6535073037434441
https://www.google.com/maps/@20.136797,-103.751916,12.77z
Distance: 0.6717687481066814
https://www.google.com/maps/@20.114109,-103.402729,12.77z
Distance: 0.5698337664677489
https://www.google.com/maps/@20.165243,-103.166678,12.77z
Distance: 0.5520160451972533
https://www.google.com/maps/@20.156780,-103.796205,12.77z
Distance: 0.6830474995609189
https://www.google.com/maps/@20.145210,-103.847824,12.77z
Distance: 0.7256412646808539
https://www.google.com/maps/@20.721356,-103.763684,12.77z
Distance: 0.40565032895145337
https://www.google.com/maps/@20.688223,-103.440808,12.77z
Distance: 0.08110711419611408
https://www.google.com/maps/@20.224779,-103.985035,12.77z
Distance: 0.7746804042284923
https://www.google.com/maps/@20.482502,-103.118562,12.77z
Distance: 0.31334228528179664
https://www.google.com/maps/@20.104911,-103.890501,12.77z
Distance: 0.784178885206701
https://www.google.com/maps/@20.118821,-103.111941,12.77z
Distance: 0.615658926755729
https://www.google.com/maps/@20.158657,-103.114359,12.77z
Distance: 0.5783886311185751
https://www.google.com/maps/@20.542731,-103.103598,12.77z
Distance: 0.29186798877851405
https://www.google.com/maps/@20.138894,-103.169292,12.77z
Distance: 0.5759011501857051
https://www.google.com/maps/@20.129048,-103.591004,12.77z
Distance: 0.5996043487300775
https://www.google.com/maps/@20.164482,-103.632888,12.77z
Distance: 0.5853912624940861
https://www.google.com/maps/@20.269915,-103.121619,12.77z
Distance: 0.47631176307920314
https://www.google.com/maps/@20.315130,-103.169353,12.77z
Distance: 0.4137043584503525
https://www.google.com/maps/@20.959015,-103.944675,12.77z
Distance: 0.6469138797401326
https://www.google.com/maps/@20.175515,-103.861202,12.77z
Distance: 0.7128479003013406
https://www.google.com/maps/@20.310842,-103.148580,12.77z
Distance: 0.42739609133685236
https://www.google.com/maps/@20.703045,-103.134298,12.77z
Distance: 0.22656588454314383
https://www.google.com/maps/@20.145167,-103.951115,12.77z
Distance: 0.7987888747460289
https://www.google.com/maps/@20.309179,-103.709312,12.77z
Distance: 0.5111960776152544
https://www.google.com/maps/@20.661185,-103.837716,12.77z
Distance: 0.4782686482294498
https://www.google.com/maps/@20.446824,-103.347909,12.77z
Distance: 0.23581393428468153
https://www.google.com/maps/@20.562951,-103.104630,12.77z
Distance: 0.2818196751811642
https://www.google.com/maps/@20.306290,-103.329792,12.77z
Distance: 0.3772466860371598
https://www.google.com/maps/@20.878617,-103.652049,12.77z
Distance: 0.35195166926330296
https://www.google.com/maps/@20.158229,-103.799811,12.77z
Distance: 0.6842459077454761
https://www.google.com/maps/@20.128576,-103.133912,12.77z
Distance: 0.598099673546827
https://www.google.com/maps/@20.108433,-103.599763,12.77z
Distance: 0.6220025660037253
https://www.google.com/maps/@20.371694,-103.120559,12.77z
Distance: 0.39215733039026135
https://www.google.com/maps/@20.936358,-103.156671,12.77z
Distance: 0.32532648532761055
https://www.google.com/maps/@20.151632,-103.627583,12.77z
Distance: 0.5943809391547008
https://www.google.com/maps/@20.307233,-103.801632,12.77z
Distance: 0.5794938779369654
https://www.google.com/maps/@20.161136,-103.250562,12.77z
Distance: 0.5325443410034694
https://www.google.com/maps/@20.151132,-103.729722,12.77z
Distance: 0.6472485531290276
https://www.google.com/maps/@20.428609,-103.226471,12.77z
Distance: 0.2866753203511193
https://www.google.com/maps/@20.738163,-103.293458,12.77z
Distance: 0.08679651799017743
https://www.google.com/maps/@20.503515,-103.723782,12.77z
Distance: 0.40543146913877265
https://www.google.com/maps/@20.131510,-103.138807,12.77z
Distance: 0.5935434084664906
https://www.google.com/maps/@20.114072,-103.182874,12.77z
Distance: 0.595200043969436
https://www.google.com/maps/@20.663985,-103.705339,12.77z
Distance: 0.3459108075351507
https://www.google.com/maps/@20.887684,-103.879933,12.77z
Distance: 0.5590957155568438
https://www.google.com/maps/@20.802457,-103.114826,12.77z
Distance: 0.272944423901315
https://www.google.com/maps/@20.124940,-103.462678,12.77z
Distance: 0.5667858208872394
https://www.google.com/maps/@20.156063,-103.163374,12.77z
Distance: 0.5617718127532002
https://www.google.com/maps/@20.745090,-103.888394,12.77z
Distance: 0.5321921661934071
https://www.google.com/maps/@20.634012,-103.140017,12.77z
Distance: 0.22514441242815197
https://www.google.com/maps/@20.106586,-103.114978,12.77z
Distance: 0.6256818317816337
https://www.google.com/maps/@20.240423,-103.262917,12.77z
Distance: 0.4524293138893841
https://www.google.com/maps/@20.161431,-103.128379,12.77z
Distance: 0.5700411577986448
https://www.google.com/maps/@20.420487,-103.140578,12.77z
Distance: 0.3415722761443248
https://www.google.com/maps/@20.132263,-103.753838,12.77z
Distance: 0.6765731785396368
https://www.google.com/maps/@20.676623,-103.177299,12.77z
Distance: 0.18270531957777347
https://www.google.com/maps/@20.101985,-103.399236,12.77z
Distance: 0.581677650984823
https://www.google.com/maps/@20.113083,-103.686214,12.77z
Distance: 0.6561368073748249
https://www.google.com/maps/@20.743183,-103.419702,12.77z
Distance: 0.08530712421844096
https://www.google.com/maps/@20.143556,-103.157655,12.77z
Distance: 0.5754900815680627
https://www.google.com/maps/@20.687414,-103.296114,12.77z
Distance: 0.06400317233402569
https://www.google.com/maps/@20.184084,-103.717593,12.77z
Distance: 0.6133389350938195
https://www.google.com/maps/@20.104757,-103.403927,12.77z
Distance: 0.5792495519908676
https://www.google.com/maps/@20.144028,-103.159228,12.77z
Distance: 0.5744968374005377
https://www.google.com/maps/@20.586953,-103.821375,12.77z
Distance: 0.4712137646809858
https://www.google.com/maps/@20.153446,-103.989591,12.77z
Distance: 0.8223212546286475
https://www.google.com/maps/@20.131221,-103.131376,12.77z
Distance: 0.5966184415286007
https://www.google.com/maps/@20.122957,-103.329024,12.77z
Distance: 0.5602274203921297
https://www.google.com/maps/@20.252149,-103.599185,12.77z
Distance: 0.49224748305260607
https://www.google.com/maps/@20.163985,-103.292647,12.77z
Distance: 0.522693708930975
https://www.google.com/maps/@20.129210,-103.160948,12.77z
Distance: 0.5878196699590917
https://www.google.com/maps/@20.333804,-103.143636,12.77z
Distance: 0.41018104001333
https://www.google.com/maps/@20.106870,-103.239313,12.77z
Distance: 0.587963855233312
https://www.google.com/maps/@20.577807,-103.363956,12.77z
Distance: 0.10460318065345844
https://www.google.com/maps/@20.120271,-103.522383,12.77z
Distance: 0.585071362026569
https://www.google.com/maps/@20.168675,-103.182175,12.77z
Distance: 0.543539473753671
https://www.google.com/maps/@20.141547,-103.826773,12.77z
Distance: 0.7144262195457473
https://www.google.com/maps/@20.422678,-103.141365,12.77z
Distance: 0.33938827006368394
https://www.google.com/maps/@20.102745,-103.880881,12.77z
Distance: 0.7793116677230163
https://www.google.com/maps/@20.805612,-103.105902,12.77z
Distance: 0.28234834640142176
https://www.google.com/maps/@20.112393,-103.802298,12.77z
Distance: 0.7214798735385607
https://www.google.com/maps/@20.357342,-103.135830,12.77z
Distance: 0.3947565036069943
https://www.google.com/maps/@20.939761,-103.404852,12.77z
Distance: 0.2613215711280286
https://www.google.com/maps/@20.210435,-103.118123,12.77z
Distance: 0.5302360725288487
https://www.google.com/maps/@20.773221,-103.156588,12.77z
Distance: 0.2227166400106156
https://www.google.com/maps/@20.144592,-103.680853,12.77z
Distance: 0.6262312279058138
https://www.google.com/maps/@20.564259,-103.685226,12.77z
Distance: 0.3460757700308177
https://www.google.com/maps/@20.547644,-103.211344,12.77z
Distance: 0.20053492473088047
https://www.google.com/maps/@20.187937,-103.183569,12.77z
Distance: 0.5249042406851127
https://www.google.com/maps/@20.127936,-103.228670,12.77z
Distance: 0.5697195028864838
https://www.google.com/maps/@20.755033,-103.148150,12.77z
Distance: 0.22389702641798867
https://www.google.com/maps/@20.183855,-103.162064,12.77z
Distance: 0.5363063275737258
https://www.google.com/maps/@20.174301,-103.117671,12.77z
Distance: 0.5628301720341732
https://www.google.com/maps/@20.386448,-103.131003,12.77z
Distance: 0.37409644499675976
https://www.google.com/maps/@20.101768,-103.630274,12.77z
Distance: 0.6404284573767962
https://www.google.com/maps/@20.130325,-103.135521,12.77z
Distance: 0.5958729315594223
https://www.google.com/maps/@20.721819,-103.165797,12.77z
Distance: 0.19809354360811185
https://www.google.com/maps/@20.126209,-103.104939,12.77z
Distance: 0.6117889455725866
https://www.google.com/maps/@20.768325,-103.747982,12.77z
Distance: 0.39748043690981677
https://www.google.com/maps/@20.105659,-103.561874,12.77z
Distance: 0.6110149438048338
https://www.google.com/maps/@20.136268,-103.139738,12.77z
Distance: 0.5887817564031846
https://www.google.com/maps/@20.348891,-103.678754,12.77z
Distance: 0.4613471700600568
https://www.google.com/maps/@20.470027,-103.195695,12.77z
Distance: 0.26840584332316125
https://www.google.com/maps/@20.130048,-103.164729,12.77z
Distance: 0.5857604806966938
https://www.google.com/maps/@20.778087,-103.144068,12.77z
Distance: 0.2361333764049797
https://www.google.com/maps/@20.131854,-103.133061,12.77z
Distance: 0.5953897221706284
https://www.google.com/maps/@20.386449,-103.481060,12.77z
Distance: 0.3197231547161102
https://www.google.com/maps/@20.180564,-103.136955,12.77z
Distance: 0.5490741592623142
https://www.google.com/maps/@20.376915,-103.573952,12.77z
Distance: 0.3729495938247209
https://www.google.com/maps/@20.347265,-103.399005,12.77z
Distance: 0.33733956451092495
https://www.google.com/maps/@20.110990,-103.137134,12.77z
Distance: 0.6132399316333006
https://www.google.com/maps/@20.982810,-103.419948,12.77z
Distance: 0.3064162487056155
https://www.google.com/maps/@20.522017,-103.193602,12.77z
Distance: 0.23099995350566255
https://www.google.com/maps/@20.780947,-103.103614,12.77z
Distance: 0.27461819379498714
https://www.google.com/maps/@20.434339,-103.139790,12.77z
Distance: 0.33159559300089453
https://www.google.com/maps/@20.824255,-103.109559,12.77z
Distance: 0.2877851392125999
https://www.google.com/maps/@20.157305,-103.135289,12.77z
Distance: 0.5710607152795723
https://www.google.com/maps/@20.465257,-103.173408,12.77z
Distance: 0.28619311205970616
https://www.google.com/maps/@20.111924,-103.117633,12.77z
Distance: 0.619730599959386
https://www.google.com/maps/@20.148946,-103.115820,12.77z
Distance: 0.586586012036785
https://www.google.com/maps/@20.117087,-103.173018,12.77z
Distance: 0.5953423796962692
https://www.google.com/maps/@20.183451,-103.118818,12.77z
Distance: 0.5540849786500387
https://www.google.com/maps/@20.139881,-103.481305,12.77z
Distance: 0.5558675235280649
https://www.google.com/maps/@20.996935,-103.140879,12.77z
Distance: 0.38334292453574637
https://www.google.com/maps/@20.146084,-103.706257,12.77z
Distance: 0.6383688091136823
https://www.google.com/maps/@20.706450,-103.627580,12.77z
Distance: 0.2687492731011935
https://www.google.com/maps/@20.158781,-103.148215,12.77z
Distance: 0.5647324026928324
https://www.google.com/maps/@20.810061,-103.113011,12.77z
Distance: 0.277986162411041
https://www.google.com/maps/@20.926002,-103.941570,12.77z
Distance: 0.6306325735799131
https://www.google.com/maps/@20.986968,-103.144068,12.77z
Distance: 0.37335372255706323
https://www.google.com/maps/@20.315407,-103.276385,12.77z
Distance: 0.37631277764117715
https://www.google.com/maps/@20.151124,-103.151613,12.77z
Distance: 0.5705890030223327
https://www.google.com/maps/@20.182842,-103.657633,12.77z
Distance: 0.581486292167257
https://www.google.com/maps/@20.287139,-103.118821,12.77z
Distance: 0.46292974773978307
https://www.google.com/maps/@20.819465,-103.121379,12.77z
Distance: 0.2751452403106688
https://www.google.com/maps/@20.664156,-103.157806,12.77z
Distance: 0.20292476170743817
https://www.google.com/maps/@20.144914,-103.455949,12.77z
Distance: 0.5459310617255825
https://www.google.com/maps/@20.700562,-103.125877,12.77z
Distance: 0.23474701596743766
https://www.google.com/maps/@20.132533,-103.162160,12.77z
Distance: 0.5842825771292683
https://www.google.com/maps/@20.644457,-103.179333,12.77z
Distance: 0.18451129515674192
https://www.google.com/maps/@20.519040,-103.605165,12.77z
Distance: 0.29463846214033584
https://www.google.com/maps/@20.810585,-103.291344,12.77z
Distance: 0.14543315341977525
https://www.google.com/maps/@20.153903,-103.330600,12.77z
Distance: 0.5292416166693807
https://www.google.com/maps/@20.769037,-103.234558,12.77z
Distance: 0.15242093755261166
https://www.google.com/maps/@20.138124,-103.101252,12.77z
Distance: 0.6025521184156727
https://www.google.com/maps/@20.106254,-103.155356,12.77z
Distance: 0.6113185770876091
https://www.google.com/maps/@20.316005,-103.297824,12.77z
Distance: 0.3715519464269004
https://www.google.com/maps/@20.120447,-103.849947,12.77z
Distance: 0.7455508893560698
https://www.google.com/maps/@20.342532,-103.112041,12.77z
Distance: 0.4206015661059955
https://www.google.com/maps/@20.803816,-103.890084,12.77z
Distance: 0.5439093678972879
https://www.google.com/maps/@20.175738,-103.133964,12.77z
Distance: 0.5546994517268031
https://www.google.com/maps/@20.570171,-103.431569,12.77z
Distance: 0.1330954309449446
https://www.google.com/maps/@20.866012,-103.890116,12.77z
Distance: 0.5611161199117634
https://www.google.com/maps/@20.154449,-103.418327,12.77z
Distance: 0.5311049965187877
https://www.google.com/maps/@20.129606,-103.183156,12.77z
Distance: 0.5803015776766598
https://www.google.com/maps/@20.622546,-103.565679,12.77z
Distance: 0.2142735641567184
https://www.google.com/maps/@20.118716,-103.187672,12.77z
Distance: 0.5893477697224425
https://www.google.com/maps/@20.120534,-103.473390,12.77z
Distance: 0.5731436627003921
https://www.google.com/maps/@20.133159,-103.501718,12.77z
Distance: 0.5671852926619496
https://www.google.com/maps/@20.142250,-103.712056,12.77z
Distance: 0.6447417220819711
https://www.google.com/maps/@20.460983,-103.161558,12.77z
Distance: 0.29722207722714467
https://www.google.com/maps/@20.182786,-103.177526,12.77z
Distance: 0.5318007990065622
https://www.google.com/maps/@20.938170,-103.821533,12.77z
Distance: 0.5277727888749348
https://www.google.com/maps/@20.184539,-103.136733,12.77z
Distance: 0.5455348019769352
https://www.google.com/maps/@20.170336,-103.113890,12.77z
Distance: 0.5680390446485344
https://www.google.com/maps/@20.213879,-103.164872,12.77z
Distance: 0.5074348408980391
https://www.google.com/maps/@20.132586,-103.480805,12.77z
Distance: 0.5628811085719809
https://www.google.com/maps/@20.232543,-103.370111,12.77z
Distance: 0.4499046464036799
https://www.google.com/maps/@20.800269,-103.177889,12.77z
Distance: 0.21689309224320774
https://www.google.com/maps/@20.931675,-103.198928,12.77z
Distance: 0.29679745306659755
https://www.google.com/maps/@20.149858,-103.177433,12.77z
Distance: 0.5628751051443138
https://www.google.com/maps/@20.323218,-103.107404,12.77z
Distance: 0.4390043193887911
https://www.google.com/maps/@20.104181,-103.169639,12.77z
Distance: 0.6086572834218088
https://www.google.com/maps/@20.159887,-103.453714,12.77z
Distance: 0.5307985645753205
https://www.google.com/maps/@20.144646,-103.135983,12.77z
Distance: 0.5824533694156279
https://www.google.com/maps/@20.112617,-103.160878,12.77z
Distance: 0.6034824457632708
https://www.google.com/maps/@20.133930,-103.489775,12.77z
Distance: 0.5635676152090542
https://www.google.com/maps/@20.453935,-103.998011,12.77z
Distance: 0.6777400776737588
https://www.google.com/maps/@20.174642,-103.677793,12.77z
Distance: 0.5989954899216027
https://www.google.com/maps/@20.167027,-103.390939,12.77z
Distance: 0.5162381509121731
https://www.google.com/maps/@20.662822,-103.777404,12.77z
Distance: 0.41794452457859504
https://www.google.com/maps/@20.153814,-103.112497,12.77z
Distance: 0.5835641337806964
https://www.google.com/maps/@20.143089,-103.114821,12.77z
Distance: 0.5923295018420193
https://www.google.com/maps/@20.178730,-103.438253,12.77z
Distance: 0.5096586128977119
https://www.google.com/maps/@20.794408,-103.146491,12.77z
Distance: 0.24106192943229146
https://www.google.com/maps/@20.737689,-103.592901,12.77z
Distance: 0.23947195237944094
https://www.google.com/maps/@20.134191,-103.213788,12.77z
Distance: 0.5672845801391211
https://www.google.com/maps/@20.122197,-103.178654,12.77z
Distance: 0.5887333153688691
https://www.google.com/maps/@20.167519,-103.180624,12.77z
Distance: 0.5451401897409707
https://www.google.com/maps/@20.115484,-103.152388,12.77z
Distance: 0.6036424982620249
https://www.google.com/maps/@20.155590,-103.437907,12.77z
Distance: 0.5324847193845285
https://www.google.com/maps/@20.317113,-103.172107,12.77z
Distance: 0.41067855243538093
https://www.google.com/maps/@20.131854,-103.659240,12.77z
Distance: 0.6265951917622896
https://www.google.com/maps/@20.165040,-103.323848,12.77z
Distance: 0.5185479268349441
https://www.google.com/maps/@20.125343,-103.170674,12.77z
Distance: 0.5882593402981504
https://www.google.com/maps/@20.105435,-103.113010,12.77z
Distance: 0.627512862333846
https://www.google.com/maps/@20.172106,-103.717081,12.77z
Distance: 0.6228147021739446
https://www.google.com/maps/@20.129988,-103.668811,12.77z
Distance: 0.6328512319981888
https://www.google.com/maps/@20.100975,-103.253784,12.77z
Distance: 0.5909652173416146
https://www.google.com/maps/@20.435338,-103.175385,12.77z
Distance: 0.30831387130782045
https://www.google.com/maps/@20.260910,-103.113200,12.77z
Distance: 0.4883287078765851
https://www.google.com/maps/@20.494096,-103.177112,12.77z
Distance: 0.26239245932919025
https://www.google.com/maps/@20.286421,-103.114647,12.77z
Distance: 0.4657274309945834
https://www.google.com/maps/@20.181522,-103.100670,12.77z
Distance: 0.5639315367453921
https://www.google.com/maps/@20.451876,-103.241586,12.77z
Distance: 0.2590594332079415
https://www.google.com/maps/@20.122972,-103.223406,12.77z
Distance: 0.5757763939714953
https://www.google.com/maps/@20.177617,-103.668331,12.77z
Distance: 0.5914877002447461
https://www.google.com/maps/@20.112954,-103.150188,12.77z
Distance: 0.606775804753304
https://www.google.com/maps/@20.166003,-103.100564,12.77z
Distance: 0.5778050991277375
https://www.google.com/maps/@20.155827,-103.120673,12.77z
Distance: 0.5783116830381526
https://www.google.com/maps/@20.606109,-103.906142,12.77z
Distance: 0.5515195257261781
https://www.google.com/maps/@20.870402,-103.372791,12.77z
Distance: 0.18851014849821682
https://www.google.com/maps/@20.133574,-103.138789,12.77z
Distance: 0.5916351953837915
https://www.google.com/maps/@20.143727,-103.116101,12.77z
Distance: 0.5912197299691706
https://www.google.com/maps/@20.119198,-103.771360,12.77z
Distance: 0.6974287922926787
https://www.google.com/maps/@20.837909,-103.822296,12.77z
Distance: 0.48785271188999185
https://www.google.com/maps/@20.593238,-103.797034,12.77z
Distance: 0.4461061436385083
https://www.google.com/maps/@20.154916,-103.122713,12.77z
Distance: 0.5783014601257854
https://www.google.com/maps/@20.155108,-103.130258,12.77z
Distance: 0.5750718522073744
https://www.google.com/maps/@20.603149,-103.181031,12.77z
Distance: 0.19562587906107126
https://www.google.com/maps/@20.520521,-103.147962,12.77z
Distance: 0.26665886199940064
https://www.google.com/maps/@20.152126,-103.201250,12.77z
Distance: 0.5534375506190548
https://www.google.com/maps/@20.453993,-103.160731,12.77z
Distance: 0.3030066835593208
https://www.google.com/maps/@20.103664,-103.132395,12.77z
Distance: 0.6217894867731509
https://www.google.com/maps/@20.658007,-103.114879,12.77z
Distance: 0.24624053442359262
https://www.google.com/maps/@20.757389,-103.115291,12.77z
Distance: 0.255879832222897
https://www.google.com/maps/@20.236498,-103.185366,12.77z
Distance: 0.4787853726186888
https://www.google.com/maps/@20.153515,-103.689443,12.77z
Distance: 0.6230859989847491
https://www.google.com/maps/@20.602099,-103.628341,12.77z
Distance: 0.2801603364618662
https://www.google.com/maps/@20.503291,-103.159004,12.77z
Distance: 0.2691114742860648
https://www.google.com/maps/@20.162310,-103.488702,12.77z
Distance: 0.5357322559824269
https://www.google.com/maps/@20.108241,-103.497049,12.77z
Distance: 0.5902425752420973
https://www.google.com/maps/@20.166478,-103.230121,12.77z
Distance: 0.5319322897527635
https://www.google.com/maps/@20.108616,-103.565587,12.77z
Distance: 0.6094678776185156
https://www.google.com/maps/@20.128036,-103.114866,12.77z
Distance: 0.6060472158883436
</pre>
